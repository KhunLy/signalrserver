﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR.Hubs
{
    public class Message
    {
        public string Author { get; set; }
        public string Recipient { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
    }
    public class ChatHub: Hub
    {
        private static Dictionary<string, string> Users 
            = new Dictionary<string, string>();

        public void Connect(string username)
        {
            Users.Add(Context.ConnectionId, username);
            Clients.All.SendAsync("REFRESH_USER", Users.Values);
        }

        public void Send(Message m)
        {
            string id1 = Users.FirstOrDefault(kvp => kvp.Value == m.Author).Key;
            string id2 = Users.FirstOrDefault(kvp => kvp.Value == m.Recipient).Key;
            Clients.Client(id1).SendAsync("NEW_MESSAGE", m);
            Clients.Client(id2).SendAsync("NEW_MESSAGE", m);
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            try
            {
                Users.Remove(Context.ConnectionId);
            }
            catch (Exception)
            {
            }

            Clients.All.SendAsync("REFRESH_USER", Users.Values);
            return base.OnDisconnectedAsync(exception);
        }
    }
}
