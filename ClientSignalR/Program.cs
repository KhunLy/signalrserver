﻿using Microsoft.AspNetCore.SignalR.Client;
using System;

namespace ClientSignalR
{
    class Program
    {
        static void Main(string[] args)
        {
            HubConnection conn = new HubConnectionBuilder()
                .WithUrl("http://localhost:2020/chathub").Build();
            conn.StartAsync().ContinueWith((arg) => {
                conn.On<string>("NEW_MESSAGE", (m) =>
                {
                    Console.WriteLine(m);
                });
                while (true)
                {
                    string message = Console.ReadLine();
                    conn.SendAsync("Send", message);
                }
            });
           
            while (true)
            {

            }
        }
    }
}
